# -*- cperl -*-
# latexmkrc

$latex         = 'platex';
$bibtex        = 'pbibtex';
# $dvipdf = "dvipdfm %O -f latexmk/allmplus-bolder.map -o %D %S";
$dvipdf = "dvipdfm %O -f latexmk/allmplus.map -o %D %S";
# $dvipdf = "dvipdfm %O -f latexmk/ipa.map -o %D %S";
